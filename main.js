$(document).ready(function () {
  $(document).on("click", "#unhide-navbar", function () {
    $(".navbar-menu").addClass("navbar-menu-unhide");
  });

  $(document).on("click", "#hide-navbar", function () {
    $(".navbar-menu").removeClass("navbar-menu-unhide");
  });

  $(document).on("click", "#play-video-btn", function () {
    $(".modal").addClass("modal-unhide");
  });

  $(document).on("click", ".btn-close", function () {
    $(".modal").removeClass("modal-unhide");
  });

  $(document).on('click', '.product-detail-btn',function(){
    window.location.href = "./product-detail.html";
  })
});

$(".owl-carousel").owlCarousel({
  loop: false,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1200: {
      items: 2,
    },
    1201: {
      items: 3,
    },
  },
});

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function () {
  output.innerHTML = this.value;
};

function openCity(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}