# Push folder test lên Gitlab
````
cd existing_folder
git init
git remote add origin git@gitlab.com:nhanlnt/test-merge-branch.git (tuỳ mỗi tài khoản có mỗi đường link khác nhau)
git add .
git commit -m "Initial commit"
git push -u origin master
````

# Sau khi push xong thì mình sẽ tạo nhánh với tên branchA và branchB (trước khi trạo nhánh thì mình sẽ viết vài dòng vào file test.txt)

# Để tạo nhánh ta dùng lệnh (tạo nhánh từ master)
````
git checkout -b tennhanh
````

# Sau khi tạo nhánh và viết thêm dòng vào nhánh A, mình sẽ push lên
````
git add .
git commit -m "add branchA"
git push origin branchA
````

#  Sau khi push nhánh A lên, mình sẽ chuyển về master với lệnh
````
git checkout master
````

# Sau khi chuyển về master xong, ta tiếp tục tạo nhánh B với lệnh:
````
git checkout -b branchB
````

# Sau khi tạo nhánh và viết thêm dòng vào nhánh B, mình sẽ push lên
````
git add .
git commit -m "add branchB"
git push origin branchB
````

# Sau khi đã push cả nhánh A và B lên thì ta trở về master để tiến hành merge nhánh A và B vào master:
1. Chuyển về master
````
git checkout master
````
2. Để gộp nhánh A
````
git merge origin/branchA
````
3. Sau khi gộp xong (có lỗi gì xảy ra), ta tiến hành push file từ master lên
````
git add .
git commit -m "merge branchA"
git push origin master
````
4. Sau khi đã push lên xong ta tiến hành gộp nhánh B
````
git merge origin/branchB
````
5. VS Code sẽ có lựa để ta lựa chọn
- Giữ dòng hiện tại
- Chèn dòng mới vào dòng hiện tại
- Viết tiếp dòng mới

# Sau khi chọn xong ở bước 5 ta tiến hành push lên
````
git add .
git commit -m "merge branchB"
git push origin master
````